void avanzar()       { motores(velocidadMotores, velocidadMotores, 0, 0); }

void retroceder()    { motores(0, 0, velocidadMotores, velocidadMotores); }

void giroIzquierda1() { motores(0, velocidadMotores, velocidadMotores, 0); }
void giroIzquierda2() { motores(0, velocidadMotores, velocidadMotores/2, 0); }
void giroIzquierda3() { motores(0, velocidadMotores, velocidadMotores/4, 0); }

void giroDerecha1 ()  { motores(velocidadMotores, 0, 0, velocidadMotores); }
void giroDerecha2 ()  { motores(velocidadMotores, 0, 0, velocidadMotores/2); }
void giroDerecha3 ()  { motores(velocidadMotores, 0, 0, velocidadMotores/4); }

void detener ()      { motores(0, 0, 0, 0); }

void motores (int izquierdaDelante,
              int derechaDelante,
              int izquierdaAtras,
              int derechaAtras) {
  analogWrite (pin_motor_izquierdo_delante, izquierdaDelante);
  analogWrite (pin_motor_derecho_delante,   derechaDelante);
  analogWrite (pin_motor_izquierdo_detras,  izquierdaAtras);
  analogWrite (pin_motor_derecho_detras,    derechaAtras);
}
