/*
  A0 Sensor3
  A1 Sensor2
  A2 Sensor1
  A3 -
  A4 Sensor7
  A5 Sensor6
  A6 Sensor5
  A7 Sensor4
*/
void leerSensores() {
  byte i = 0;
  
  for (i = 0; i < SENSORES; i++)
    sensorValue[i] = analogRead(i);
  
  if (DEBUG){
    for (i = 0; i < SENSORES; i++) {
      Serial.print(sensorValue[i]);
      Serial.print(" ");
    }
    Serial.println("");
  }

//  if (DEBUG){
//    for (i = 0; i < SENSORES; i++) {
//      Serial.print(linea[i]);
//      Serial.print(" ");
//    }
//    Serial.println("");
//  }
}


void leerBotones() {
  boolean switch1 = true, switch2 = true, switch3 = true;
  boolean button = true, pulsado = false;

  button = digitalRead(bt1);
  
  while(!button){
    switch1 = digitalRead(sw1);
    switch2 = digitalRead(sw2);
    switch3 = digitalRead(sw3);
    button = digitalRead(bt1);
    pulsado = true;
  }

  if (pulsado) {
    PORTB &= ~B00100000; // 13 OFF
    delay(250);

    if ( switch1 &&  switch2 && !switch3) velocidadMotores = 50;
    if ( switch1 && !switch2 &&  switch3) velocidadMotores = 100;
    if ( switch1 && !switch2 && !switch3) velocidadMotores = 150;
    if (!switch1 &&  switch2 &&  switch3) velocidadMotores = 200;
    if (!switch1 &&  switch2 && !switch3) velocidadMotores = 255;

    corre();
  }

  detener();
}

void calibrar() {
  unsigned long fin = 2500L;
  int lineaMax[SENSORES] = {0, 0, 0, 0, 0, 0, 0};
  int lineaMin[SENSORES] = {1024, 1024, 1024, 1024, 1024, 1024, 1024};
  int i = 0;
  int temp = 0;

  // Lectura de valores máximos y mínimos
  fin += millis();
  while (millis() <= fin) {
    for (i = 0; i < SENSORES; i++) {
      temp = analogRead(i);
      if (temp > lineaMax[i]) lineaMax[i] = temp;
      if (temp < lineaMin[i]) lineaMin[i] = temp;
    }
  }

  // Cálculo de la media
  for (i = 0; i < SENSORES; i++)
    linea[i] = (lineaMax[i] + lineaMin[i]) / 2;

  // Indica que se hay acabado encendiendo el LED 13
  PORTB |= B00100000; // 13 ON
}
