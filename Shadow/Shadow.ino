// Alberto Ferreiro Campello "Kampy"
// Proyecto KR-Shadow
// http://oshwdem.org

// Optimización
#pragma GCC optimize("-O2")

// Librerías

// Pines de los motores
#define pin_motor_izquierdo_delante 9
#define pin_motor_izquierdo_detras  6
#define pin_motor_derecho_delante   5
#define pin_motor_derecho_detras   10

// Pines de los interruptores
#define sw1 4
#define sw2 3
#define sw3 2
#define bt1 11

// Pines de los sensores
#define sensor1 A2
#define sensor2 A1
#define sensor3 A0
#define sensor4 A7
#define sensor5 A6
#define sensor6 A5
#define sensor7 A4

// Constantes
const boolean DEBUG = true;
const byte SENSORES = 7;

// Variables globales
volatile double sensorValue[SENSORES] = {0, 0, 0, 0, 0, 0, 0};
volatile byte velocidadMotores = 255;
// Valores de disparo (Indican que han encontrado la línea)
volatile int linea[SENSORES] = {0, 0, 0, 0, 0, 0, 0};


void setup() {
  unsigned long temp = 1000L;
  
  // Inicialización de pines
  DDRD |= B01100000; // Digitales 0~7 como entradas, 5 y 6 como salidas
  DDRB |= B00100110; // Digitales 8~13 como entradas, 9, 10, y 13 como salidas
  DDRC |= B00000000; // Analogicos A0~A8 como entradas
  pinMode (sw1, INPUT_PULLUP);
  pinMode (sw2, INPUT_PULLUP);
  pinMode (sw3, INPUT_PULLUP);
  pinMode (bt1, INPUT_PULLUP);

  PORTB |= B00100000; // 13 ON
  temp += millis();
  while (millis() <= temp) {}
  PORTB &= ~B00100000; // 13 OFF
  calibrar();

  if (DEBUG)
    Serial.begin(115200);
}


void loop() {
  leerSensores();
  leerBotones();
}
